﻿using System;

namespace REST
{
    public interface ITextToSpeech
    {
        void Speak(string text);
    }
}
